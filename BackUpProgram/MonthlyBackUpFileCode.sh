#!/bin/bash
filename="MonthlyBackUpFile_$(date '+%d-%m-%Y').tar"
tar -cvf /home/ubuntu/PracticingCode/$filename -C /home/ubuntu/PracticingCode/ mistralassignment
chmod 777 "/home/ubuntu/PracticingCode/$filename" 
rsync -av -e ssh /home/ubuntu/PracticingCode/$filename kali@192.168.111.100:BackUpDirectory/MonthlyBackUp
rm -rf ~/PracticingCode/$filename
