# MAKEFILE

___


I organized all the cprogram file in CProgram Directory.I created Makefile file in CProgram Directory

This is a Makefile for compiling and running a C program.

The first line defines the variable `"CC"` as the compiler `"gcc"`. The second line defines the variable `"cc"` as the compiler `"clang"`.

The `"compile"` target is defined to compile the C program using both compilers and generate executables named `"result"` and `"Result"` respectively.

The `"gccOutput"` and `"clangOutput"` targets are defined to execute the previously generated executables respectively.

The `"removeExecutableFile"` target is defined to remove the generated executable files `"result"` and `"Result"`.

The `"gccCompileAndOutput"` and `"clangCompileAndOutput"` targets are defined to compile and execute the program using both compilers respectively.

To run these targets, navigate to the directory containing the Makefile in a terminal and enter the desired target command, such as `"make compile"` or `"make gccCompileAndOutput"`. 


## Code

---

```
$(CC) = gcc
cc = clang
compile:
	$(CC) main.c add.c sub.c multiply.c divide.c -o result
	cc main.c add.c sub.c multiply.c divide.c -o Result
gccOutput:
	./result
clangOutput:
	./Result
removeExecutableFile:
	rm result
	rm Result
gccCompileAndOutput:
	$(CC) main.c add.c sub.c multiply.c divide.c -o result
	./result
clangCompileAndOutput:
	cc main.c add.c sub.c multiply.c divide.c -o Result
	./Result

```
___

# BackUpProgram

___

## DailyBackUpFileCode.sh

```
#!/bin/bash
filename="DailyBackUpFile_$(date '+%d-%m-%Y').tar"
tar -cvf /home/ubuntu/PracticingCode/$filename -C /home/ubuntu/PracticingCode/ mistralassignment
chmod 777 "/home/ubuntu/PracticingCode/$filename"
rsync -av -e ssh /home/ubuntu/PracticingCode/$filename kali@192.168.111.100:~/BackUpDirectory/DailyBackUp
rm -rf ~/PracticingCode/$filename

```

The first line (#!/bin/bash) is the shebang, which specifies the interpreter to be used to execute the script.
The filename variable is assigned a string value that includes the current date in the format of day-month-year. This variable will be used as the name of the backup file.
The tar command creates a compressed tarball archive of the mistralassignment directory, with the filename specified by the filename variable. The -C option changes to the /home/ubuntu/PracticingCode/ d>
The chmod command sets the permissions of the backup file to 777, which means it will be readable, writable, and executable by everyone.
The rsync command transfers the backup file to a remote server with the IP address 192.168.111.100, using the kali user account and the ~/BackUpDirectory/DailyBackUp directory. The -av option preserves >
The rm command removes the backup file from the local directory after the transfer is complete.


## WeeklyBackUpFileCode.sh

```
#!/bin/bash
filename="WeeklyBackUpFile_$(date '+%d-%m-%Y').tar"
tar -cvf /home/ubuntu/PracticingCode/$filename -C /home/ubuntu/PracticingCode/ mistralassignment
chmod 777 "/home/ubuntu/PracticingCode/$filename"
rsync -av -e ssh /home/ubuntu/PracticingCode/$filename kali@192.168.111.100:~/BackUpDirectory/DailyBackUp
rm -rf ~/PracticingCode/$filename

```

The first line (#!/bin/bash) is the shebang, which specifies the interpreter to be used to execute the script.
The filename variable is assigned a string value that includes the current date in the format of day-month-year. This variable will be used as the name of the backup file.
The tar command creates a compressed tarball archive of the mistralassignment directory, with the filename specified by the filename variable. The -C option changes to the /home/ubuntu/PracticingCode/ directory before adding the contents of mistralassignment to the archive.
The chmod command sets the permissions of the backup file to 777, which means it will be readable, writable, and executable by everyone.
The rsync command transfers the backup file to a remote server with the IP address 192.168.111.100, using the kali user account and the ~/BackUpDirectory/WeeklyBackUp directory. The -av option preserves the permissions and timestamps of the backup file during the transfer, and the -e ssh option specifies that the transfer should use the SSH protocol.
The rm command removes the backup file from the local directory after the transfer is complete.


## MonthlyBackUpFileCode.sh

```
#!/bin/bash
filename="MonthlyBackUpFile_$(date '+%d-%m-%Y').tar"
tar -cvf /home/ubuntu/PracticingCode/$filename -C /home/ubuntu/PracticingCode/ mistralassignment
chmod 777 "/home/ubuntu/PracticingCode/$filename"
rsync -av -e ssh /home/ubuntu/PracticingCode/$filename kali@192.168.111.100:~/BackUpDirectory/DailyBackUp
rm -rf ~/PracticingCode/$filename

```

The first line (#!/bin/bash) is the shebang, which specifies the interpreter to be used to execute the script.
The filename variable is assigned a string value that includes the current date in the format of day-month-year. This variable will be used as the name of the backup file.
The tar command creates a compressed tarball archive of the mistralassignment directory, with the filename specified by the filename variable. The -C option changes to the /home/ubuntu/PracticingCode/ d>
The chmod command sets the permissions of the backup file to 777, which means it will be readable, writable, and executable by everyone.
The rsync command transfers the backup file to a remote server with the IP address 192.168.111.100, using the kali user account and the ~/BackUpDirectory/MonthlyBackUp directory. The -av option preserves >
The rm command removes the backup file from the local directory after the transfer is complete.


## crontab -e

```
* * * * * /bin/bash /home/ubuntu/PracticingCode/DailyBackUpFileCode.sh
* * * * 1 /bin/bash /home/ubuntu/PracticingCode/WeeklyBackUpFileCode.sh
* * 20 * * /bin/bash /home/ubuntu/PracticingCode/MonthlyBackUpFileCode.sh

```

Each line represents a separate scheduled task and follows the cron format, which consists of five fields representing the minute, hour, day of the month, month, and day of the week. An asterisk (*) in a field indicates that the task should be executed for all possible values of that field.

The first line `"* * * * * /bin/bash /home/ubuntu/PracticingCode/DailyBackUpFileCode.sh"` specifies a task to be executed every minute, using the Bash shell to execute the script "DailyBackUpFileCode.sh" located in the directory "/home/ubuntu/PracticingCode/". This script likely performs a daily backup of some kind.

The second line `"* * * * 1 /bin/bash /home/ubuntu/PracticingCode/WeeklyBackUpFileCode.sh"` specifies a task to be executed every minute on Mondays, using the Bash shell to execute the script "WeeklyBackUpFileCode.sh" located in the directory "/home/ubuntu/PracticingCode/". This script likely performs a weekly backup of some kind.

The third line `"* * 20 * * /bin/bash /home/ubuntu/PracticingCode/MonthlyBackUpFileCode.sh"` specifies a task to be executed every minute on the 20th day of the month, using the Bash shell to execute the script "MonthlyBackUpFileCode.sh" located in the directory "/home/ubuntu/PracticingCode/". This script likely performs a monthly backup of some kind.

___


# fakeWebsites

___


Step 1: Install apache server in your ubuntu Using command line - `sudo apt-get install apache2`

Step 2: To Start the apache server,type the command - `service apache2 start` or `systemctl start apache2`

Step 3: To Check apache server running in your system, open browser(firefox) type in the url field - `http://localhost:80`

Step 4: If ubuntu page shows ,it means it works properly.

Step 5: Now We have to set up specific port to listen.To make it listen port.Configure the `ports.conf` file in `/etc/apache2/`

Step 6: Once We configured.Now mentioned ports are listened.To check it is either listen or not.Type the command - `ss -nat`.It shows the listening ports

Step 7: Then set up our html file.In my case, I set up all the html files in `/var/www` within each directory for each port.

Step 8: One more thing we need to do .That is what we need to make each port fetch particular html file in my case.So we need to configure `000-default.conf` to make virtualhost for each port I need to use

Step 9: then It works perfectly.Browse the url with `http://localhost:8081` and `http://localhost:8081` check what we configured is working perfectly.


___
